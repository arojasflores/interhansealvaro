USE [pont_grup_alvaro]
GO

/****** Object:  Table [dbo].[USUARIO]    Script Date: 19/11/2018 18:43:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[USUARIO](
	[ID_USUARIO] [int] NOT NULL,
	[DS_LOGIN] [nchar](20) NOT NULL,
	[DS_PASSWORD] [nchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[ID_USUARIO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[DS_LOGIN] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[FRASE](
	[ID_FRASE] [int] NOT NULL,
	[DS_FRASE] [nchar](500) NOT NULL,
	[ID_USUARIO] [int] NOT NULL,
	[FH_FRASE] [datetime] NOT NULL,
 CONSTRAINT [PK__FRASE__0377EFD8A948E49E] PRIMARY KEY CLUSTERED 
(
	[ID_FRASE] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [FRASE_USUARIO] UNIQUE NONCLUSTERED 
(
	[DS_FRASE] ASC,
	[ID_USUARIO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[FRASE]  WITH CHECK ADD  CONSTRAINT [FK_FRASE_USUARIO] FOREIGN KEY ([ID_USUARIO])
REFERENCES [dbo].[USUARIO] ([ID_USUARIO])
GO

ALTER TABLE [dbo].[FRASE] CHECK CONSTRAINT [FK_FRASE_USUARIO]
GO
