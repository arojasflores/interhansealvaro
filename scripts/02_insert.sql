USE [pont_grup_alvaro]
GO

INSERT INTO [dbo].[USUARIO]
           ([ID_USUARIO]
           ,[DS_LOGIN]
           ,[DS_PASSWORD])
     VALUES
           (1
           ,'usr'
           ,'1234')
GO

INSERT INTO [dbo].[USUARIO]
           ([ID_USUARIO]
           ,[DS_LOGIN]
           ,[DS_PASSWORD])
     VALUES
           (2
           ,'usr2'
           ,'12345')
GO

INSERT INTO [dbo].[FRASE]
           ([ID_FRASE]
           ,[DS_FRASE]
           ,[ID_USUARIO]
           ,[FH_FRASE])
     VALUES
           (1
           ,'Chuck Norris lucho la guerra del golfo el solo, en ambos bandos y gano'
           ,1
           ,GETDATE())
GO


INSERT INTO [dbo].[FRASE]
           ([ID_FRASE]
           ,[DS_FRASE]
           ,[ID_USUARIO]
           ,[FH_FRASE])
     VALUES
           (2
           ,'Las armas no matan a la gente, Chuck Norris mata a la gente.'
           ,1
           ,GETDATE())
GO

INSERT INTO [dbo].[FRASE]
           ([ID_FRASE]
           ,[DS_FRASE]
           ,[ID_USUARIO]
           ,[FH_FRASE])
     VALUES
           (3
           ,'Chuck Norris tiene dos velocidades: Andar y Matar.'
           ,2
           ,GETDATE())
GO
