﻿using ChuckNorris.pont_grup_alvaroDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChuckNorris
{
    public partial class frmLogin : Form
    {
        USUARIOTableAdapter uSUARIOTableAdapter = new USUARIOTableAdapter();
        private const String ERROR_TITLE = "Error Inicio de Sesión";

        public frmLogin()
        {
            InitializeComponent();
        }

        /*
         COMPRUEBA QUE EL USUARIO Y CONTRASEÑA INTRODUCIDOS COINCIDEN Y EXISTEN EN BBDD, DE SER ASÍ
         SE ABRE UN FORMULARIO PARA EL CRUD DE LAS FRASES DE CHUCK NORRISS
             */
        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            string dsLogin = this.txtLogin.Text;
            string dsPassword = this.txtPassword.Text;

            if (String.IsNullOrEmpty(dsLogin) || String.IsNullOrEmpty(dsPassword))
            {
                MessageBox.Show(this, "Usuario/Contraseña no proporcionados",
                    ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int? idUsuario = uSUARIOTableAdapter.existeUsuario(dsLogin, dsPassword);
                if (idUsuario > 0)
                {
                    Program.IdUsuario = idUsuario.Value;
                    this.Close();
                }
                else
                {
                    MessageBox.Show(this, "Usuario/Contraseña incorrectos",
                        ERROR_TITLE, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
