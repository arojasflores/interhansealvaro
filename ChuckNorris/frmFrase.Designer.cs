﻿namespace ChuckNorris
{
    partial class frmFrase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFrase));
            this.txtFrase1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // txtFrase1
            // 
            this.txtFrase1.BackColor = System.Drawing.Color.AntiqueWhite;
            this.txtFrase1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFrase1.Location = new System.Drawing.Point(12, 12);
            this.txtFrase1.Multiline = true;
            this.txtFrase1.Name = "txtFrase1";
            this.txtFrase1.ReadOnly = true;
            this.txtFrase1.Size = new System.Drawing.Size(350, 105);
            this.txtFrase1.TabIndex = 1;
            // 
            // frmFrase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 131);
            this.Controls.Add(this.txtFrase1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFrase";
            this.Text = "Frase";
            this.Load += new System.EventHandler(this.frmFrase_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.TextBox txtFrase1;
    }
}