﻿using ChuckNorris.pont_grup_alvaroDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChuckNorris
{
    public partial class frmPanelControl : Form
    {
        USUARIOTableAdapter uSUARIOTableAdapter = new USUARIOTableAdapter();

        /* el indice seleccionado del datagrid actualmente */
        private int indexActual;

        /*este flag controla que solo se accede al datagrid ANTES de una operacion en BBDD de insercion o modificacion*/
        private bool acceder;

        public frmPanelControl()
        {
            InitializeComponent();
        }

        /* al cargarse el formulario del usuario, imprime por pantalla el nombre del usuario */
        private void frmPanelControl_Load(object sender, EventArgs e)
        {
            cargarDatagrid();

            if (Program.IdUsuario > -1) 
                this.nombreUsuario.Text = uSUARIOTableAdapter.getUsuarioByID(Program.IdUsuario).ToString();

            indexActual = 0; acceder = true;
            Program.IdFrase = -1;
        }

        private void btnCerrarSesion_Click(object sender, EventArgs e)
        {
            Program.IdUsuario = -1;
            this.Close();
        }

        /*
         PARA CREAR UNA NUEVA FRASE DEL MAESTRO CHUCK NORRIS Y QUE SE MUESTRE TAMBIEN EN EL DATAGRID
        */
        private void btnCrear_Click(object sender, EventArgs e)
        {
            string frase = this.txtFrase.Text;
            if (String.IsNullOrEmpty(frase))
            {
                MessageBox.Show(this, "La frase es obligatoria",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                try
                {
                    //if (!existeFrase(frase))
                    //{
                        fRASETableAdapter.Insert(frase, Program.IdUsuario, -3);
                        indexActual = dgFrases.Rows.Count;
                        //cuando se actualiza el datagrid, el flag de acceso al mismo se desactiva para no perder el foco en la fila actual, a 
                        //menos que se trate de una operacion de eliminacion
                        acceder = false;
                        updateDataGrid();
                    //}
                } catch (Exception ex)
                {
                    mensajeException(ex, "Error Inserción");
                }
                
            }
        }

        /* Método controlador de excepciones, si la excepcion es de duplicidad de datos, en función del código de la 
			excepción, muestra un mensaje personalizado, ya que se trata de una excepción controlada
			*/
        private void mensajeException(Exception ex, String titulo)
        {
            if (ex.HResult == -2146232060)
            {
                MessageBox.Show(this, "Ya existe esta frase proporcionada por usted. Introduzca otra distinta",
                    titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                MessageBox.Show(this, "Error Inesperado: " + ex.Message,
                titulo, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /*
        ABRE UNA NUEVA VENTANA PARA MOSTRAR UNA FRASE ELEGIDA POR EL USUARIO
        */
        private void btnLeer_Click(object sender, EventArgs e)
        {
            int idFrase = getIdFrase();
            Program.IdFrase = idFrase;

            frmFrase frm = new frmFrase();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog(this);
        }

        /*
        MODIFICAR UNA FRASE DE CHUCK NORRIS CREADA POR EL PROPIO USUARIO Y ACTUALIZARLA EN EL DATAGRID
        */
        private void btnModificar_Click(object sender, EventArgs e)
        {
            int index = getIdFrase();
            string frase = this.txtFrase.Text;

            if (String.IsNullOrEmpty(frase))
            {
                MessageBox.Show(this, "La frase es obligatoria",
                "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            { try
                {
                    fRASETableAdapter.Update(frase, index, Program.IdUsuario);
                    indexActual = this.dgFrases.CurrentRow.Index;
                    //cuando se actualiza el datagrid, el flag de acceso al mismo se desactiva para no perder el foco en la fila actual, a 
                    //menos que se trate de una operacion de eliminacion
                    acceder = false;
                    updateDataGrid();    
                }
                catch (Exception ex)
                {
                    mensajeException(ex, "Error Modificación");
                }
            }
        }

        /*
        ELIMINA UNA FRASE CREADA POR EL USUARIO
        */
        private void btnEliminar_Click(object sender, EventArgs e)
        {
            int index = getIdFrase();
            try
            {
                fRASETableAdapter.Delete(index, Program.IdUsuario);
                indexActual = 0;
                updateDataGrid();
            }
            catch (Exception ex)
            {
                mensajeException(ex, "Error Eliminación");
            }
        }

        /*
        METODO PRIVADO QUE OBTIENE EL ID DE LA FRASE SELECCIONADA POR EL USUARIO
        */
        private int getIdFrase()
        {
            int idFrase = -1;
            if (dgFrases.CurrentRow != null)
            {
                int index = dgFrases.CurrentRow.Index;
                if (index > -1)
                {
                    idFrase = int.Parse(this.dgFrases.Rows[index].Cells[0].Value.ToString());
                }
                else
                {
                    MessageBox.Show(this, "Por favor, seleccione una frase",
                    "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            return idFrase;
        }

        /*
        METODO QUE ACTUALIZA EL DATAGRID DESPUES DE UNA OPERACION DE BASE DE DATOS
        */
        private void updateDataGrid()
        {
            cargarDatagrid();
            this.dgFrases.ClearSelection();

            this.txtFrase.Text = "";

            //hay que comprobar que haya datos en el datagrid o lanzará una excepción
            if (indexActual > -1 && dgFrases.Rows.Count > 0)
                this.dgFrases.CurrentCell = dgFrases.Rows[indexActual].Cells[1];

            //al final del metodo, se reactiva el flag de acceso a las filas del datagrid
            if (!acceder) acceder = true;
        }

        /* Este método actualiza el datagrid y activa o desactiva los botones de lectura, modificacion y eliminacion
           en funcion de si hay datos o no en el datagrid */
        private void cargarDatagrid()
        {
            this.fRASETableAdapter.FillByUsuario(this.pont_grup_alvaroDataSet.FRASE, Program.IdUsuario);
            activarBoton(btnModificar);
            activarBoton(btnLeer);
            activarBoton(btnEliminar);
        }

        /* Método que activa un botón en función de si hay o no datos en el datagrid */
        private void activarBoton(Button button)
        {
            button.Enabled = this.fRASETableAdapter.getNumeroFrases() > 0;
        }

        /* SI EL FLAG DE ACCESO ESTÁ ACTIVADO, SE PODRÁ CAMBIAR DE FILA EN EL DATAGRID */
        private void dgFrases_SelectionChanged(object sender, EventArgs e)
        {
            if (dgFrases.CurrentRow != null && acceder)
            {
                int idFrase = getIdFrase();
                //this.txtFrase.Text = "";
                if (idFrase > -1)
                    //es muy importante usar la funcion TRIM(), de lo contrario no se puede editar el textbox de la frase
                    this.txtFrase.Text = fRASETableAdapter.getFrase(idFrase, Program.IdUsuario).Trim();
            }
        }
    }
}
