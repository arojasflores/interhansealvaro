﻿using ChuckNorris.pont_grup_alvaroDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChuckNorris
{
    public partial class frmFrase : Form
    {
        FRASETableAdapter fRASETableAdapter = new FRASETableAdapter();

        public frmFrase()
        {
            InitializeComponent();
        }

        /*
         SE MUESTRA LA FRASE DE CHUCK NORRIS ELEGIDA POR EL USUARIO
         */
        private void frmFrase_Load(object sender, EventArgs e)
        {
            this.txtFrase1.Text = fRASETableAdapter.getFrase(Program.IdFrase, Program.IdUsuario);
        }
    }
}
