﻿namespace ChuckNorris
{
    partial class frmPanelControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPanelControl));
            this.dgFrases = new System.Windows.Forms.DataGridView();
            this.txtFrase = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nombreUsuario = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnLeer = new System.Windows.Forms.Button();
            this.btnCerrarSesion = new System.Windows.Forms.Button();
            this.btnEliminar = new System.Windows.Forms.Button();
            this.btnModificar = new System.Windows.Forms.Button();
            this.btnCrear = new System.Windows.Forms.Button();
            this.iDFRASEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dSFRASEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.iDUSUARIODataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fHFRASEDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.fRASEBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.pont_grup_alvaroDataSet = new ChuckNorris.pont_grup_alvaroDataSet();
            this.fRASETableAdapter = new ChuckNorris.pont_grup_alvaroDataSetTableAdapters.FRASETableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.dgFrases)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRASEBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pont_grup_alvaroDataSet)).BeginInit();
            this.SuspendLayout();
            // 
            // dgFrases
            // 
            this.dgFrases.AllowUserToAddRows = false;
            this.dgFrases.AutoGenerateColumns = false;
            this.dgFrases.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFrases.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iDFRASEDataGridViewTextBoxColumn,
            this.dSFRASEDataGridViewTextBoxColumn,
            this.iDUSUARIODataGridViewTextBoxColumn,
            this.fHFRASEDataGridViewTextBoxColumn});
            this.dgFrases.DataSource = this.fRASEBindingSource;
            this.dgFrases.Location = new System.Drawing.Point(155, 82);
            this.dgFrases.MultiSelect = false;
            this.dgFrases.Name = "dgFrases";
            this.dgFrases.ReadOnly = true;
            this.dgFrases.Size = new System.Drawing.Size(553, 180);
            this.dgFrases.TabIndex = 0;
            this.dgFrases.SelectionChanged += new System.EventHandler(this.dgFrases_SelectionChanged);
            // 
            // txtFrase
            // 
            this.txtFrase.Location = new System.Drawing.Point(155, 290);
            this.txtFrase.MaxLength = 500;
            this.txtFrase.Multiline = true;
            this.txtFrase.Name = "txtFrase";
            this.txtFrase.Size = new System.Drawing.Size(553, 83);
            this.txtFrase.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(152, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Frase";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 18);
            this.label2.TabIndex = 8;
            this.label2.Text = "Hola, ";
            // 
            // nombreUsuario
            // 
            this.nombreUsuario.AutoSize = true;
            this.nombreUsuario.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nombreUsuario.Location = new System.Drawing.Point(63, 26);
            this.nombreUsuario.Name = "nombreUsuario";
            this.nombreUsuario.Size = new System.Drawing.Size(46, 18);
            this.nombreUsuario.TabIndex = 9;
            this.nombreUsuario.Text = "label3";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pictureBox1.Image = global::ChuckNorris.Properties.Resources.icondd;
            this.pictureBox1.Location = new System.Drawing.Point(25, 290);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 83);
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // btnLeer
            // 
            this.btnLeer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLeer.Image = global::ChuckNorris.Properties.Resources.read_transp;
            this.btnLeer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLeer.Location = new System.Drawing.Point(12, 130);
            this.btnLeer.Name = "btnLeer";
            this.btnLeer.Size = new System.Drawing.Size(137, 36);
            this.btnLeer.TabIndex = 10;
            this.btnLeer.Text = "Leer";
            this.btnLeer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLeer.UseVisualStyleBackColor = true;
            this.btnLeer.Click += new System.EventHandler(this.btnLeer_Click);
            // 
            // btnCerrarSesion
            // 
            this.btnCerrarSesion.BackColor = System.Drawing.Color.LightCoral;
            this.btnCerrarSesion.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCerrarSesion.Image = global::ChuckNorris.Properties.Resources.login_transp;
            this.btnCerrarSesion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCerrarSesion.Location = new System.Drawing.Point(550, 15);
            this.btnCerrarSesion.Name = "btnCerrarSesion";
            this.btnCerrarSesion.Size = new System.Drawing.Size(158, 43);
            this.btnCerrarSesion.TabIndex = 7;
            this.btnCerrarSesion.Text = "Cerrar Sesión";
            this.btnCerrarSesion.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCerrarSesion.UseVisualStyleBackColor = false;
            this.btnCerrarSesion.Click += new System.EventHandler(this.btnCerrarSesion_Click);
            // 
            // btnEliminar
            // 
            this.btnEliminar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEliminar.Image = global::ChuckNorris.Properties.Resources.delete_transp;
            this.btnEliminar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnEliminar.Location = new System.Drawing.Point(12, 220);
            this.btnEliminar.Name = "btnEliminar";
            this.btnEliminar.Size = new System.Drawing.Size(137, 42);
            this.btnEliminar.TabIndex = 4;
            this.btnEliminar.Text = "Eliminar";
            this.btnEliminar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEliminar.UseVisualStyleBackColor = true;
            this.btnEliminar.Click += new System.EventHandler(this.btnEliminar_Click);
            // 
            // btnModificar
            // 
            this.btnModificar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModificar.Image = global::ChuckNorris.Properties.Resources.update_transp;
            this.btnModificar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnModificar.Location = new System.Drawing.Point(12, 172);
            this.btnModificar.Name = "btnModificar";
            this.btnModificar.Size = new System.Drawing.Size(137, 42);
            this.btnModificar.TabIndex = 3;
            this.btnModificar.Text = "Modificar";
            this.btnModificar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnModificar.UseVisualStyleBackColor = true;
            this.btnModificar.Click += new System.EventHandler(this.btnModificar_Click);
            // 
            // btnCrear
            // 
            this.btnCrear.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCrear.Image = global::ChuckNorris.Properties.Resources.insert_transp;
            this.btnCrear.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCrear.Location = new System.Drawing.Point(12, 82);
            this.btnCrear.Name = "btnCrear";
            this.btnCrear.Size = new System.Drawing.Size(137, 42);
            this.btnCrear.TabIndex = 1;
            this.btnCrear.Text = "Crear";
            this.btnCrear.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCrear.UseVisualStyleBackColor = true;
            this.btnCrear.Click += new System.EventHandler(this.btnCrear_Click);
            // 
            // iDFRASEDataGridViewTextBoxColumn
            // 
            this.iDFRASEDataGridViewTextBoxColumn.DataPropertyName = "ID_FRASE";
            this.iDFRASEDataGridViewTextBoxColumn.HeaderText = "ID_FRASE";
            this.iDFRASEDataGridViewTextBoxColumn.Name = "iDFRASEDataGridViewTextBoxColumn";
            this.iDFRASEDataGridViewTextBoxColumn.ReadOnly = true;
            this.iDFRASEDataGridViewTextBoxColumn.Visible = false;
            // 
            // dSFRASEDataGridViewTextBoxColumn
            // 
            this.dSFRASEDataGridViewTextBoxColumn.DataPropertyName = "DS_FRASE";
            this.dSFRASEDataGridViewTextBoxColumn.HeaderText = "Frase";
            this.dSFRASEDataGridViewTextBoxColumn.Name = "dSFRASEDataGridViewTextBoxColumn";
            this.dSFRASEDataGridViewTextBoxColumn.ReadOnly = true;
            this.dSFRASEDataGridViewTextBoxColumn.Width = 400;
            // 
            // iDUSUARIODataGridViewTextBoxColumn
            // 
            this.iDUSUARIODataGridViewTextBoxColumn.DataPropertyName = "ID_USUARIO";
            this.iDUSUARIODataGridViewTextBoxColumn.HeaderText = "ID_USUARIO";
            this.iDUSUARIODataGridViewTextBoxColumn.Name = "iDUSUARIODataGridViewTextBoxColumn";
            this.iDUSUARIODataGridViewTextBoxColumn.ReadOnly = true;
            this.iDUSUARIODataGridViewTextBoxColumn.Visible = false;
            // 
            // fHFRASEDataGridViewTextBoxColumn
            // 
            this.fHFRASEDataGridViewTextBoxColumn.DataPropertyName = "FH_FRASE";
            this.fHFRASEDataGridViewTextBoxColumn.HeaderText = "Fecha";
            this.fHFRASEDataGridViewTextBoxColumn.Name = "fHFRASEDataGridViewTextBoxColumn";
            this.fHFRASEDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // fRASEBindingSource
            // 
            this.fRASEBindingSource.DataMember = "FRASE";
            this.fRASEBindingSource.DataSource = this.pont_grup_alvaroDataSet;
            // 
            // pont_grup_alvaroDataSet
            // 
            this.pont_grup_alvaroDataSet.DataSetName = "pont_grup_alvaroDataSet";
            this.pont_grup_alvaroDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // fRASETableAdapter
            // 
            this.fRASETableAdapter.ClearBeforeFill = true;
            // 
            // frmPanelControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(730, 385);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnLeer);
            this.Controls.Add(this.nombreUsuario);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnCerrarSesion);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFrase);
            this.Controls.Add(this.btnEliminar);
            this.Controls.Add(this.btnModificar);
            this.Controls.Add(this.btnCrear);
            this.Controls.Add(this.dgFrases);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPanelControl";
            this.Text = "Panel de Control";
            this.Load += new System.EventHandler(this.frmPanelControl_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgFrases)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.fRASEBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pont_grup_alvaroDataSet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgFrases;
        private System.Windows.Forms.Button btnCrear;
        private System.Windows.Forms.Button btnModificar;
        private System.Windows.Forms.Button btnEliminar;
        private System.Windows.Forms.TextBox txtFrase;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCerrarSesion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label nombreUsuario;
        private pont_grup_alvaroDataSet pont_grup_alvaroDataSet;
        private System.Windows.Forms.BindingSource fRASEBindingSource;
        private pont_grup_alvaroDataSetTableAdapters.FRASETableAdapter fRASETableAdapter;
        private System.Windows.Forms.Button btnLeer;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDFRASEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn dSFRASEDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDUSUARIODataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn fHFRASEDataGridViewTextBoxColumn;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}