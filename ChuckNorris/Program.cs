﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChuckNorris
{
    static class Program
    {
        /*
         * ALMACENA GLOBALMENTE EL ID DEL USUARIO, PARA CONTROLAR EL ACCESO AL CRUD DE FRASES
         */
        private static int idUsuario;
        public static int IdUsuario { get => idUsuario; set => idUsuario = value; }

        /*
         * ALMACENA GLOBALMENTE EL ID DE LA FRASE, PARA MOSTRARSE CUANDO LA ELIGE EL USUARIO
         */
        private static int idFrase;
        public static int IdFrase { get => idFrase; set => idFrase = value; }

        /// <summary>
        /// Punto de entrada principal para la aplicación.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            frmPrincipal frm = new frmPrincipal();
            frm.StartPosition = FormStartPosition.CenterScreen;
            Application.Run(frm);
        }
    }
}
