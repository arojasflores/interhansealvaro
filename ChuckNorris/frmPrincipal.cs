﻿using ChuckNorris.pont_grup_alvaroDataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChuckNorris
{
    public partial class frmPrincipal : Form
    {
        FRASETableAdapter fRASETableAdapter = new FRASETableAdapter();
 
        public frmPrincipal()
        {
            InitializeComponent();
        }

        /*
         MUESTRA UNA FRASE ALEATORIA DEL MAESTRO CHUCK NORRIS EN EL FORMULARIO PRINCIPAL DE CUALQUIER USUARIO
             */
        private void btnRefrescar_click(object sender, EventArgs e)
        {
            this.txtFrase.Text = fRASETableAdapter.getFraseRandom();
        }

        /*
        CARGA EL FORMULARIO RRINCIPAL SIN USUARIO ASIGNADO Y CON UNA FRASE ALEATORIA DE CHUCK NORRIS PARA EMPEZAR
             */
        private void frmPrincipal_Load(object sender, EventArgs e)
        {
            Program.IdUsuario = -1;
            setFrase();
        }

        /*
         BOTON QUE ABRE UN FORMULARIO DE INICIO DE SESION
             */
        private void btnIniciarSesion_Click(object sender, EventArgs e)
        {
            /* 
             *  Se muestra un formulario de inicio de sesión
             */
            frmLogin frm = new frmLogin();
            frm.StartPosition = FormStartPosition.CenterScreen;
            frm.ShowDialog(this);

            /* 
             *  Una vez iniciada sesión, se muestra el formulario privado
             *  del usuario para el CRUD de sus propias frases
             */
            frmPanelControl frm2 = new frmPanelControl();
            frm2.StartPosition = FormStartPosition.CenterScreen;
            frm2.ShowDialog(this);

            /*
             * una vez que el usuario cierra sesión, se actualiza de
             * nuevo de forma automática la frase aleatoria, y si sus
             * frases fueran las unicas de BBDD y el usuario las borra
             * todas, el botón se deshabilitará.
             */
            setFrase();
        }

        /* Metodo que imprime una frase en el cuadro de texto, si hubiera, y activa o desactiva
         el botón de refrescar frase */
        private void setFrase()
        {
            this.txtFrase.Text = fRASETableAdapter.getFraseRandom();
            btnRefrescar.Enabled = fRASETableAdapter.getNumeroFrases() > 0;
        }

    }
}
